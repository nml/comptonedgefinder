function new_legend = generate_legend(qq,act_title,LEGH,channels)
if qq==1
        new_legend = act_title;
else
         leg_new = {char(LEGH.String); strcat('Channel', num2str(channels(qq)))};
         new_legend=  (cell2mat(leg_new));
end