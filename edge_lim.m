function [x_lim] = edge_lim(yph)
    first_der = diff(smooth(yph,3,'rlowess'));
    first_der_sign = num2str(sign(first_der'));
%     figure
%     subplot (2,1,1)    
%     plot (yph)
%     hold on
%     subplot (2,1,2)
%     plot (sign(first_der'),'*')
    valley = ['-1',' -1',' -1' ,'  1'];
    index_v=strfind (first_der_sign, valley);
    index_vall = fix((index_v(1)-1)/3); %subtract one because 
    % the first value of the derivative is always positive
    index_vall_res = rem((index_v(1)-1),3);
    index_valley = index_vall+index_vall_res+1+2;
    [max_edge, x_lim(1)] =max(yph(index_valley:end));
    x_lim(1)=x_lim(1)+index_valley-1;
    x_lim(2)=length(yph)-find((flipud(abs(first_der))>1),1);
    
    % check that I have at least 5 data points to determine 5 coefficients
    check_len = x_lim(2)-x_lim(1);
    if check_len < 5
          x_lim(2) = x_lim(2)+(5- check_len);
    end
    
%     figure (500)
%     plot (yph,'r')
%     hold on
%     plot (index_valley,yph(index_valley),'d')
%       hold on    
%     plot (x_lim(1),yph(x_lim(1)),'o')
%       hold on
%     plot (x_lim(2),yph(x_lim(2)),'*')
%     hold on
%     
%     close ('gcf')
end