%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SINGLE Compton edge calculator 
% for pulse height and integral distributions
% for multiple folders, for multiple channels
% builds the distribution plotting the count rate
% also builds the normalized distribution
% designed for 0-2 V negative pulses
% Tested with Matlab 2017b
% Requires pulse fitting plugin
%
%     REQUIRED DATA FORMAT 
%     FILENAME:  basenamen.dat
%     basename is the file name
%     n is the channel number
%     Event format (this format is the output by wavedump.exe
%     program used for CAEN Tech digitizers)
%       Header (6x4 byte int) - not used in this program
%       Samples (2 byte unsigned int each)
%
% The script is able to iterate through n folders, each with a
% basenamen.dat file in it
% data are expected in the folder root/m/basenamen.dat
% m goes from 1 to k (k>0)
% where root is the folder where this program is executed from
% 
% the user nees to select the correct number of points in each sample
% according to the data settings, the sample and sampling rate, see
% "definitions from user"
% 
% The binning of the PHD is a critical parameter to allow the algorithm to
% correctly identify the edge - if the binning is too fine and the distribution 
% noisy, the algorithm will mislocate the egde. You can adjust the bin
% editing the two variables:
% xph       = [0:.02:dyn_range]';
% xpi       = [0:.7:int_range]';
% This script is designed for single-point calibration with Cs-137, but the
% variable cEdgeEnergy can be changed to any other energy
% By default, the calibration point is the minimum of the derivative of the Compton edge, 
% if use_deriv = 0 then it is controlled by the value 'perc', i.e., a fraction of
% the edge between 0 and 1, typ 0.5-0.8.
% 
%
% Output
% Figures: 
% 0. A number of pulses decided by the user
% 1. Pulse Height Distribution - Counts vs Volts (saved as PHDn.png)
% 2. Pulse Integral Distribution - Counts vs Volts ns/dt (saved as PIDn.png)
% 3. Calibrated PHD and PID overlapped
%
% Print out
% compton_h: Counts at edge, voltage at edge
% compton_i: Counts at edge, pulse integral at edge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start the clock
tic;
clear all
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% definitions from user 
resolution  = 14;               % vertical resolution of the digitizer (bit)
dyn_range   = 2;               % dynamic range of the ditizer (V)
int_range   = 10;                % V ns/dt
pointsize   = 2;                 % point size for binary data (kB)
nrpoints    = 208;                % number of points per pulse
dt          = 2e-9;                       % fixed pulse time step (s)
dt_ns       = 2;
offset_pts  = 5;
time=(0:dt:dt*nrpoints-dt)*1e9;       % pulse time scale (ns)
pulses2plot = 10;               %number of sample pulses to check visually

basename    = 'wave';
folders     = [1:1];                                        
channels    = [0];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% run-dependent settings
xph       = [0:.02:dyn_range]';                   %pulse height spectrum binning in V
xpi       = [0:.15:int_range]';                   %pulse integral spectrum binning in V ns/dt
keVee_bin = 0:5:2000;                      %LO binning (for figure)
use_deriv = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% extra options
% 1 if zero suppression enabled, consider changing the threshold 
zero_sup = 1;    
min_volt = 0.0001;   
clipped_headroom = 0.01;
perc = 0.8;
C = {'b','r'};
cEdgeEnergy = 478; % keVee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% digitizer data format parameters
pre  = 5;
post = 60;
header_size   = 24;    % header size (B); 4 Bytes TTT + 4 Bytes Event counter
header_size_w = 6;
precision     = strcat (num2str(nrpoints),'*uint16');       
ph_precision  = 0.0001;                                    % 0.1 mV
pi_precision  = 0.001;                                     %10 V

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set data path
recpmax_n=0;
x_lim= zeros(2,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% read in and process data
path = '';

for qq=1:length(channels);
    
    if (exist( strcat(path , '1\',basename , num2str(channels(qq)) , ...
            '.dat') , 'file' ) == 2)
        ltestl = dir(strcat(path , '1\',basename , num2str(channels(qq)) , ...
            '.dat'));
        
        if(ltestl.bytes ~= 0)
            
            for tt = 1:length(folders)
                
                if( exist(strcat(path , num2str(folders(tt))) , 'dir') ==7)
                    filen=strcat( path , num2str(folders(tt)),'\',basename);
                    file = strcat(filen,num2str(channels(qq)),'.dat');    
                    
                    if(exist(file , 'file') == 2)
                        test=dir(file);
                        filesize=test.bytes;
                        
                        if(filesize ~= 0)
                            
                            %%% display current folder to terminal
                            disp(strcat ('folder' , num2str(folders(tt))));
                            disp(strcat('channel' , num2str(channels(qq)) ))
                            [fid,message] = fopen(file);
                            fseek(fid,0,'bof');
                            nrpulses =  floor(filesize / ((nrpoints * pointsize) + header_size));   
                            
                            %%% skip header and convert from ADC units to volts
                            Header(:,:) = fread(fid,[header_size_w,nrpulses],'6*int32', 2*nrpoints); %skips the signal
                            fseek(fid,24,'bof');  %reads after the first header
                            pulses_array_raw(:,:) = fread(fid,[nrpoints,nrpulses],precision, header_size);
                            baseline = mean (pulses_array_raw((1:offset_pts),:));
                            pulses_array_volts = abs(bsxfun(@minus,pulses_array_raw,baseline)).* dyn_range ./ (2^resolution-1) ;
                            fclose(fid);
                            
                            %%% plot raw data  
                            figure(qq)
                            plot(time, pulses_array_volts(:,1:pulses2plot))
                            xlabel('Time (ns)')
                            ylabel('Voltage (V)')
                            titl=strcat('Channel  ',num2str(channels(qq)));
                            title(titl)    
                            
                            %%% find max value (PH) and sum (PI) for all
                            %%% non-anamalous pulses
                            hlimit = dyn_range - clipped_headroom;
                            clipped_pulses = -99*ones(1,nrpulses);
                            anomal_pulses = -99*ones(1,nrpulses);
                            recpmax = -99*ones(1,nrpulses);
                            recpint = -99*ones(1,nrpulses);
                            index_clipped = 1;
                            index2 = 1;     %index for good pulses
                            anomal = 1;
                            
                            for ii = 1 : nrpulses
                                
                                % read a single pulse
                                readpulse = pulses_array_volts(:,ii); 
                                
                                % find max of each pulse (pmax) and 
                                % the index position of maximum (indmax)
                                [pmax,indmax] = max(readpulse);       
                     
                                if (pmax > hlimit)
                                    clipped_pulses(index_clipped) = ii;
                                    index_clipped = index_clipped + 1;
                                else         
                                    if zero_sup ==1
                                        check = pmax>min_volt;   
                                    else
                                        check = 1;
                                    end
                                    
                                    %%% if pulse is ok, add it to proper
                                    %%% array
                                    if indmax-pre > 0 && indmax + post < nrpoints && check    
                                        recpmax(index2) = pmax;
                                        recpint(index2) = sum(readpulse(indmax-pre:indmax+post));
                                        index2 = index2 + 1;
                                    else 
                                        %anomalous pulse
                                        anomal_pulses(anomal) = ii;
                                        anomal = anomal+1;
                                    end
                                    
                                end
                                
                            end
                            
                            %%% fill PH and PI arrays with good pulse data 
                            %%% from  current folder
                            recpmax(index2:end) = [];
                            recpint(index2:end) = [];
                            
                            if tt==1
                                pulse_max = recpmax;
                                pulse_int = recpint;
                            else
                                 pulse_max = [pulse_max, recpmax];
                                 pulse_int = [pulse_int, recpint];
                            end
                            
                            clearvars Header  pulses_array_raw  pulses_array_volts
                        end
                    end
                end
            end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot pulse height spectrum
            [yph,xph]=hist(pulse_max,xph);
            [ypi,xpi]=hist(pulse_int,xpi);
            figure(max(channels)+qq)
            yph=yph'; 
            ypi=ypi'; 
            hold on            
            figure
            hnew=plot(xph, yph(:,qq) , 'LineWidth' , 2.5);
                title ('Pulse height distribution');
                titl = strcat('Channel', num2str((qq)-1));
                xlab =  xlabel('V');
                ylab =  ylabel('Counts');
                [LEGH,OBJH,OUTH,OUTM] = legend;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
%% find the peak value associated with the PH Compton edge

            x_lim = edge_lim(yph);   
            [fit_res, gof]=fit_interp(xph(x_lim(1):x_lim(2),1), yph(x_lim(1):x_lim(2)));
            max_rel = max(yph(x_lim(1):x_lim(2)));
            compton_edge = perc.*max_rel;
            max_ph=max_rel;
            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fit the PH Compton edge and find the 80% location

            fit_x = [xph(x_lim(1)):ph_precision:xph(x_lim(2))];
            fitted = fit_res(fit_x);
            if logical(use_deriv)   %exploit the fine interpolation to calculate the derivative precisely
               deriv=diff(fitted);
               x_der=linspace(1,length(deriv),length(deriv));
               [fit_der,gof]=fit_deriv(x_der,deriv);
               [~,x_edge_eff]=min(fit_der(x_der));
               x_edge=fit_x(x_edge_eff-1);
               compton_edge=fit_res(fit_x(x_edge_eff-1));
%                % debug
%                figure
%                plot (fit_der(x_der),'.')
%                figure
%                plot( x_edge,compton_edge,'*')           
%                hold on
%                plot (xph, yph)    
               cross_pt_single=(x_edge);
            else
               fitted_shift = fitted-compton_edge;

               fitted_cross = num2str(fitted_shift>0);
               cross_pt = strfind (fitted_cross', ['1' '0']);
               cross_pt_single=fit_x(cross_pt(1));
            end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% format figure

                set( gca                       , ...
                    'FontName'   , 'Helvetica' );
                set([xlab, ylab ]  , ...
                    'FontSize'   , 16 , ...
                    'Interpreter','latex'      );     
                set(gca, ...
                    'Box'         , 'off'     , ...
                    'TickDir'     , 'out'     , ...
                    'TickLength'  , [.02 .02] , ...
                    'XMinorTick'  , 'on'      , ...
                    'YMinorTick'  , 'on'      , ...
                    'YGrid'       , 'off'     , ...
                    'XColor'      , [0 0 0], ...
                     'YColor'      , [0 0 0], ...
                     'LineWidth'   , 1.5    , ...       
                     'FontSize' , 16         , ...       
                    'FontName'   ,  'serif' );
        hold on
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add marker for Compton edge location

        compton_h(:,qq) = double([compton_edge; cross_pt_single(:,qq)])
        plot  (cross_pt_single(:,qq) , compton_edge,'d' , 'MarkerSize' , 9)
        [LEGH,OBJH,OUTH,OUTM] = legend;
        leg_new = generate_legend(qq, titl, LEGH, channels);
        legend(leg_new)
        saveas(gcf , ['PH.png']);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot pulse integral

         figure
            hnew1=plot (xpi, ypi(:,qq) , 'LineWidth' , 2.5);
            title(['Channel ' , num2str((qq)-1) ]);
            title ('Pulse integral distribution');
            xlab = xlabel('V ns');
            ylab = ylabel ('Counts');
            [LEGH,OBJH,OUTH,OUTM] = legend;
            leg_new = generate_legend(qq, titl, LEGH, channels);
            legend(leg_new)
            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
%% find the peak value associated with the PI Compton edge

            x_lim=[];
            x_lim = edge_lim(ypi);
            
            [fit_res, gof]=fit_interp(xpi(x_lim(1):x_lim(2),1), ypi(x_lim(1):x_lim(2)));
            max_rel = max(ypi(x_lim(1):x_lim(2)));
            compton_edge = perc.*max_rel;
            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fit the PI Compton edge and find the 80% location

            fit_x = [xpi(x_lim(1)):pi_precision:xpi(x_lim(2))];
            fitted = fit_res(fit_x);
            if logical(use_deriv)   %exploit the fine interpolation to calculate the derivative precisely
               deriv=diff(fitted);
               x_der=linspace(1,length(deriv),length(deriv));
               [fit_der,gof]=fit_deriv(x_der,deriv);
               [~,x_edge_eff]=min(fit_der(x_der));
               x_edge=fit_x(x_edge_eff-1);
               compton_edge=fit_res(fit_x(x_edge_eff-1));
%                % debug
%                figure
%                plot (fit_der(x_der),'.')
%                figure
%                plot( x_edge,compton_edge,'*')           
%                hold on
%                plot (xpi, ypi)    
               cross_pt_single=(x_edge);
            else
            fitted_shift = fitted-compton_edge;
            fitted_cross = num2str(fitted_shift>0);
            cross_pt = strfind (fitted_cross', ['1' '0']);
            cross_pt_single=fit_x(cross_pt(1));
            end
              compton_int(:,qq) = double([compton_edge; cross_pt_single(:,qq)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% format figure

                xlim ([0 18])
                set( gca                       , ...
                    'FontName'   , 'Helvetica' );
                set([xlab, ylab ]  , ...
                    'FontSize'   , 16 , ...
                    'Interpreter','latex'      );     
                set(gca, ...
                    'Box'         , 'off'     , ...
                    'TickDir'     , 'out'     , ...
                    'TickLength'  , [.02 .02] , ...
                    'XMinorTick'  , 'on'      , ...
                    'YMinorTick'  , 'on'      , ...
                    'YGrid'       , 'off'     , ...
                    'XColor'      , [0 0 0], ...
                    'YColor'      , [0 0 0], ...
                    'LineWidth'   , 1.5    , ...       
                    'FontSize' , 16         , ...       
                    'FontName'   ,  'serif' );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add marker for Compton edge location

              hold on
            plot ( compton_int(2,qq),compton_int(1,qq),'d' , 'MarkerSize' , 9)
            [LEGH,OBJH,OUTH,OUTM] = legend;
            leg_new = generate_legend(qq, titl, LEGH, channels);
            legend(leg_new)
            saveas(gcf , ['PI',num2str(qq-1),'.png']);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Light output calibration
                         
           % find conversion coefficient LO/V
           norm_coeff_int(qq) = cEdgeEnergy./compton_int(2,qq); % keVee/(V ns)
           norm_coeff_h(qq) = cEdgeEnergy./compton_h(2,qq); % keVee/(V ns)

           yph_keVee = pulse_max.*norm_coeff_h(qq);
           ypi_keVee = pulse_int.*norm_coeff_int(qq);

           % create light output histograms
            [yph_k_binned(:,qq),keVee_bin]=hist(yph_keVee,keVee_bin);
            [ypi_k_binned(:,qq),keVee_bin]=hist(ypi_keVee,keVee_bin);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot PH and PI spectra on same axes to verify accuracy

            figure
            plot(keVee_bin,yph_k_binned(:,qq));
            hold on
            plot(keVee_bin,ypi_k_binned(:,qq),'m');
            title(['Channel ' , num2str(qq) ]);
            legend('Pulse height distribution','Pulse integral distribution');
            xlabel ('keVee');
            set( gca                       , ...
                    'FontName'   , 'Helvetica' );
            set([xlab, ylab ]  , ...
                    'FontSize'   , 16 , ...
                    'Interpreter','latex'      );     
            set(gca, ...
                    'Box'         , 'off'     , ...
                    'TickDir'     , 'out'     , ...
                    'TickLength'  , [.02 .02] , ...
                    'XMinorTick'  , 'on'      , ...
                    'YMinorTick'  , 'on'      , ...
                    'YGrid'       , 'off'     , ...
                    'XColor'      , [0 0 0], ...
                     'YColor'      , [0 0 0], ...
                     'LineWidth'   , 1.5    , ...       
                     'FontSize' , 16         , ...       
                    'FontName'   ,  'serif' );

        end   
    end
end
    